using Godot;
using System;

public partial class KillZone : Area2D
{
    [Export] private NodePath pathToTimer;
	Timer timer;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
	{
        timer = GetNode<Timer>(pathToTimer);
    }

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}

	public void OnBodyEntered(Node2D body)
	{
		GD.Print("You died !");
		Engine.TimeScale = 0.5;
		body.GetNode<CollisionShape2D>("CollisionShape2D").QueueFree();
		timer.Start();
	}

	public void OnTimerTimeout()
	{
        Engine.TimeScale = 1.0;
        GetTree().ReloadCurrentScene();
	}
}
