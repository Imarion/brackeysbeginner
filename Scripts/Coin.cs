using Godot;
using System;

public partial class Coin : Area2D
{
    [Export] private NodePath pathToGameManager;
    GameManager gameManager;

    [Export] private NodePath pathToAnimationPlayer;
    AnimationPlayer animationPlayer;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
	{
		GD.Print("I'm a coin.");
        gameManager = GetNode<GameManager>(pathToGameManager);
        animationPlayer = GetNode<AnimationPlayer>(pathToAnimationPlayer);
    }

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{

	}

    public void OnBodyEntered(Node2D body)
    {
        GD.Print("Blip");
        gameManager.IncreaseScore();
        animationPlayer.Play("PickupAnimation");
		//QueueFree();
    }

}
