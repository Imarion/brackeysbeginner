using Godot;
using System;

public partial class Player : CharacterBody2D
{
    [Export] private NodePath pathToAnimatedSprite;
    AnimatedSprite2D animatedSprite2D;

    public const float Speed = 130.0f;
	public const float JumpVelocity = -300.0f;

	// Get the gravity from the project settings to be synced with RigidBody nodes.
	public float gravity = ProjectSettings.GetSetting("physics/2d/default_gravity").AsSingle();

    public override void _Ready()
    {
        animatedSprite2D = GetNode<AnimatedSprite2D>(pathToAnimatedSprite);
    }

    public override void _PhysicsProcess(double delta)
	{
		Vector2 velocity = Velocity;

		// Add the gravity.
		if (!IsOnFloor())
			velocity.Y += gravity * (float)delta;

		// Handle Jump.
		if (Input.IsActionJustPressed("Jump") && IsOnFloor())
			velocity.Y = JumpVelocity;

		// Get the input direction and handle the movement/deceleration.
		// As good practice, you should replace UI actions with custom gameplay actions.
		Vector2 direction = Input.GetVector("MoveLeft", "MoveRight", "ui_up", "ui_down");

		// Animation
		if (IsOnFloor()) {
			if (direction.X == 0)
			{
                //GD.Print("Idle");
                animatedSprite2D.Play("Idle");
            }
            else {
                //GD.Print("Run");
                animatedSprite2D.Play("Run");
            }
        }
		else
		{
			//GD.Print("Jump");
			animatedSprite2D.Play("Jump");
		}

		// Movement
		if (direction != Vector2.Zero)
		{
			// Moving orientation
			if (direction.X < 0) { animatedSprite2D.FlipH = true; }
            if (direction.X > 0) { animatedSprite2D.FlipH = false; }

            velocity.X = direction.X * Speed;
		}
		else
		{
			velocity.X = Mathf.MoveToward(Velocity.X, 0, Speed);
        }

		Velocity = velocity;
		MoveAndSlide();
	}
}
