using Godot;
using System;

public partial class GreenSlime : Node2D
{

    [Export] private NodePath pathToRightCast;
    [Export] private NodePath pathToLeftCast;
    [Export] private NodePath pathToAnimatedSprite;
    
	RayCast2D rightCast;
	RayCast2D leftCast;
	AnimatedSprite2D animatedSprite2D;

    private const double SPEED = 60;
	private int direction = 1;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
        rightCast = GetNode<RayCast2D>(pathToRightCast);
        leftCast = GetNode<RayCast2D>(pathToLeftCast);
        animatedSprite2D = GetNode<AnimatedSprite2D>(pathToAnimatedSprite);
    }

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if (rightCast.IsColliding()) { 
			direction = -1;
			animatedSprite2D.FlipH = true;
		}

		if (leftCast.IsColliding()) { 
			direction = 1;
            animatedSprite2D.FlipH = false;
        }

        Vector2 NewPos = Position;
		NewPos.X += (float)(SPEED * delta * direction);
		Position = NewPos;
    }
}
