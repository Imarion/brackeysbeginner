using Godot;
using System;

public partial class GameManager : Node
{
    [Export] private NodePath pathToScoreLabel;
    Label scoreLabel;

    private int score = 0;

    public override void _Ready()
    {
        scoreLabel = GetNode<Label>(pathToScoreLabel);
    }

    public void IncreaseScore()
	{ 
		score++;
		scoreLabel.Text = "Score : " + score.ToString();
		GD.Print("Score : " +  score);
	}

}
